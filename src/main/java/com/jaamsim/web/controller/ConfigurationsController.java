package com.jaamsim.web.controller;

import com.jaamsim.web.dao.ConfigurationDao;
import com.jaamsim.web.model.Configuration;
import com.jaamsim.web.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpSession;
import java.util.Calendar;
import java.util.List;

import static com.jaamsim.web.constants.WebConstants.*;

/**
 * Created by tristano on 06/01/16.
 */
@Controller
@RequestMapping("/sim")
public class ConfigurationsController {

    private final static String CONFIGURATION_FILE = "configurationFile";

    @Autowired
    private ConfigurationDao configurationDao;

    @RequestMapping(path = "/configurations", method = RequestMethod.GET)
    public String getConfigurations(HttpSession session, Model model) {

        User user = (User) session.getAttribute("user");

        List<Configuration> configurations = configurationDao.findAllByUser(user);

        model.addAttribute("configurations", configurations);

        return WEB_JSP + WEB_CONFIGURATIONS;
    }

    @RequestMapping(path = "/configurations/upload", method = RequestMethod.POST)
    public @ResponseBody String uploadConfiguration(MultipartHttpServletRequest request, HttpSession session) {

        MultipartFile file = request.getFileMap().get(CONFIGURATION_FILE);

        User user = (User) session.getAttribute("user");
        String fileName = file.getOriginalFilename();

        Configuration configuration = configurationDao.findByUserAndName(user, fileName);

        if (!file.isEmpty()) {
            try {

                byte[] bytes = file.getBytes();

                if(configuration == null) {
                    configuration = new Configuration();
                    configuration.setUser(user);
                }

                configuration.setName(fileName);
                configuration.setFile(bytes);
                configuration.setUploadedTime(Calendar.getInstance().getTime());

                configurationDao.save(configuration);

                return "File uploaded!";
            } catch (Exception e) {
                return "Uploading file [" + fileName + "] failed: " + e.getMessage();
            }
        } else {
            return "File [" + fileName + "] empty!";
        }
    }
}
