package com.jaamsim.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

import static com.jaamsim.web.constants.WebConstants.WEB_REDIRECT;
import static com.jaamsim.web.constants.WebConstants.WEB_SIM;
import static com.jaamsim.web.constants.WebConstants.WEB_MAIN;

/**
 * Created by tristano on 06/01/16.
 */
@Controller
@RequestMapping("/sim")
public class LoginController {

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(HttpSession session) {

        return WEB_REDIRECT + WEB_SIM + WEB_MAIN;
    }
}
