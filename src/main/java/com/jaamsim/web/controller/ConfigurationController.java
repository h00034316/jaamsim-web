package com.jaamsim.web.controller;

import com.jaamsim.web.dao.ConfigurationDao;
import com.jaamsim.web.dao.ReportDao;
import com.jaamsim.web.dao.SimulationDao;
import com.jaamsim.web.model.Configuration;
import com.jaamsim.web.model.simulation.Report;
import com.jaamsim.web.model.User;
import com.jaamsim.web.model.simulation.Simulation;
import com.jaamsim.web.model.simulation.State;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.List;

import static com.jaamsim.web.constants.WebConstants.WEB_CONFIGURATION;
import static com.jaamsim.web.constants.WebConstants.WEB_JSP;

/**
 * Created by tristano on 06/01/16.
 */
@Controller
@RequestMapping("/sim")
public class ConfigurationController {

    private final static String CONFIGURATION_FILE = "configurationFile";

    @Autowired
    private ConfigurationDao configurationDao;

    @Autowired
    private SimulationDao simulationDao;

    @Autowired
    private ReportDao reportDao;

    @RequestMapping(path = "/configuration/{id}", method = RequestMethod.GET)
    public String getConfigurations(@PathVariable("id") int configurationId, HttpSession session, Model model) {

        User user = (User) session.getAttribute("user");

        Configuration configuration = configurationDao.findByIdAndUser(configurationId, user);
        Simulation simulation = simulationDao.findByUserAndConfigurationAndState(user, configuration, State.RUNNING);
        List<Simulation> simulations = simulationDao.findByUserAndConfigurationOrderByStartDesc(user, configuration);
        List<Report> reports = reportDao.getAllByConfiguration(configuration);

        model.addAttribute("currentSimulation", simulation);
        model.addAttribute("configuration", configuration);
        model.addAttribute("simulations", simulations);
        model.addAttribute("reports", reports);

        return WEB_JSP + WEB_CONFIGURATION;
    }

}
