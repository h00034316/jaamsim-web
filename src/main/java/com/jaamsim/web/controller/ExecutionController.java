package com.jaamsim.web.controller;

import com.jaamsim.web.dao.ConfigurationDao;
import com.jaamsim.web.dao.SimulationDao;
import com.jaamsim.web.model.Configuration;
import com.jaamsim.web.model.User;
import com.jaamsim.web.model.simulation.Simulation;
import com.jaamsim.web.model.simulation.State;
import com.jaamsim.web.process.ProcessSimulation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import java.util.Calendar;

import static com.jaamsim.web.constants.WebConstants.WEB_CONFIGURATION;
import static com.jaamsim.web.constants.WebConstants.WEB_SIM;
import static com.jaamsim.web.constants.WebConstants.WEB_REDIRECT;

/**
 * Created by tristano on 06/01/16.
 */
@Controller
@RequestMapping("/sim")
public class ExecutionController {
    private static final Logger LOG = LogManager.getLogger(ExecutionController.class);

    @Autowired
    ServletContext context;

    @Autowired
    private ConfigurationDao configurationDao;

    @Autowired
    private SimulationDao simulationDao;

    @Autowired
    private ProcessSimulation processSimulation;

    @RequestMapping(path = "/execute/{id}", method = RequestMethod.POST)
    public String execute(@PathVariable("id") int configurationId, HttpSession session) {

        User user = (User) session.getAttribute("user");

        Configuration configuration = configurationDao.findByIdAndUser(configurationId, user);

        Simulation simulation = simulationDao.findByUserAndConfigurationAndState(user, configuration, State.RUNNING);

        if(simulation == null) {
            LOG.info("Starting a new session with configuration file " + configuration.getName());

            simulation = new Simulation();
            simulation.setUser(user);
            simulation.setStart(Calendar.getInstance().getTime());
            simulation.setConfiguration(configuration);
            simulation.setState(State.NEW);
            simulationDao.save(simulation);

            processSimulation.execute(simulation);

            // TODO register listener for updating state and for registering completion
            simulation.setState(State.RUNNING);
            simulationDao.save(simulation);
        }

        return WEB_REDIRECT + WEB_SIM + WEB_CONFIGURATION + "/" + configurationId;
    }

    @RequestMapping(path = "/executing/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ExecutingData executing(@PathVariable("id") int configurationId, HttpSession session) {

        User user = (User) session.getAttribute("user");

        Configuration configuration = configurationDao.findByIdAndUser(configurationId, user);

        Simulation simulation = simulationDao.findByUserAndConfigurationAndState(user, configuration, State.RUNNING);

        if(simulation != null) {
            return new ExecutingData(simulation.getProgress(), simulation.getState().equals(State.RUNNING));
        } else {
            return new ExecutingData(0, false);
        }
    }

    private static class ExecutingData {
        private int progress;
        private boolean executing;

        public ExecutingData(int progress, boolean executing) {
            this.progress = progress;
            this.executing = executing;
        }

        public int getProgress() {
            return progress;
        }

        public void setProgress(int progress) {
            this.progress = progress;
        }

        public boolean isExecuting() {
            return executing;
        }

        public void setExecuting(boolean executing) {
            this.executing = executing;
        }
    }
}
