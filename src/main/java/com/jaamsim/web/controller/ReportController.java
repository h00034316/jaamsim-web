package com.jaamsim.web.controller;

import com.jaamsim.web.dao.ReportDao;
import com.jaamsim.web.model.User;
import com.jaamsim.web.model.simulation.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by tristano on 06/01/16.
 */
@Controller
@RequestMapping("/sim/configuration")
public class ReportController {

    @Autowired
    private ReportDao reportDao;

    @RequestMapping(path = "/report/{id}", method = RequestMethod.GET, produces= MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody byte[] getReport(@PathVariable("id") int reportId, HttpSession session, HttpServletResponse response) {

        User user = (User) session.getAttribute("user");

        Report report = reportDao.findByIdAndUser(reportId, user);

        response.setHeader("Content-Disposition", "attachment;filename=".concat(report.getName()));

        return report.getFile();
    }

}
