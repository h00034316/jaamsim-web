package com.jaamsim.web.controller;

import com.jaamsim.web.dao.ConfigurationDao;
import com.jaamsim.web.dao.SimulationDao;
import com.jaamsim.web.model.Configuration;
import com.jaamsim.web.model.User;
import com.jaamsim.web.model.simulation.Simulation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpSession;
import java.util.Calendar;
import java.util.List;

import static com.jaamsim.web.constants.WebConstants.WEB_CONFIGURATIONS;
import static com.jaamsim.web.constants.WebConstants.WEB_JSP;
import static com.jaamsim.web.constants.WebConstants.WEB_MAIN;

/**
 * Created by tristano on 10/01/16.
 */
@Controller
@RequestMapping("/sim")
public class MainController {

    @Autowired
    private SimulationDao simulationDao;

    @RequestMapping(path = "/main", method = RequestMethod.GET)
    public String getConfigurations(HttpSession session, Model model) {

        User user = (User) session.getAttribute("user");

        List<Simulation> simulations = simulationDao.findAllByUserOrderByStartDesc(user);

        model.addAttribute("simulations", simulations);

        return WEB_JSP + WEB_MAIN;
    }
}
