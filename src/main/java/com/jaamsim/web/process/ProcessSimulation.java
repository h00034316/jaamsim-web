package com.jaamsim.web.process;

import com.jaamsim.engine.JaamSim;
import com.jaamsim.events.EventTimeListener;
import com.jaamsim.events.ProgressListener;
import com.jaamsim.events.ReportListener;
import com.jaamsim.web.dao.ReportDao;
import com.jaamsim.web.dao.SimulationDao;
import com.jaamsim.web.model.simulation.Report;
import com.jaamsim.web.model.simulation.Simulation;
import com.jaamsim.web.model.simulation.State;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import java.util.Calendar;
import java.util.concurrent.Callable;

/**
 * Created by tristano on 10/01/16.
 */
@Service
public class ProcessSimulation {

    private static final Logger LOG = LogManager.getFormatterLogger(ProcessSimulation.class.getSimpleName());

    @Autowired
    ServletContext context;

    @Autowired
    private SimulationDao simulationDao;

    @Autowired
    private ReportDao reportDao;

    @Autowired
    private MailService mailService;

    @Async
    public void execute(Simulation simulation) {
        WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
        JaamSim jaamSim = (JaamSim) ctx.getBean("jaamSim");
        LOG.debug("Got JaamSim bean: " + jaamSim);

        jaamSim.addConfiguration(simulation.getConfiguration().getFile());
        jaamSim.init();

        simulation.setState(State.RUNNING);
        simulationDao.save(simulation);

        jaamSim.addProgressListener(new ProcessUpdate(simulation));
        jaamSim.addReportListener(new ProcessReport(simulation));
    }

    private class ProcessUpdate implements ProgressListener {
        Simulation simulation;

        ProcessUpdate(Simulation simulation) {
            this.simulation = simulation;
        }

        public void update(int progress) {
            if (progress == 100) {
                simulation.setEnd(Calendar.getInstance().getTime());
                simulation.setState(State.FINISHED);
            }
            simulation.setProgress(progress);
            simulationDao.save(simulation);
        }
    }

    private class ProcessReport implements ReportListener {
        Simulation simulation;

        ProcessReport(Simulation simulation) {
            this.simulation = simulation;
        }
        public void save(byte[] data) {
            Report report = new Report();
            report.setSimulation(simulation);
            report.setConfiguration(simulation.getConfiguration());
            report.setUser(simulation.getUser());
            report.setState(State.FINISHED);
            report.setFile(data);
            report.setExecutionTime(Calendar.getInstance().getTime());
            Calendar.getInstance().getTimeInMillis();
            report.setName(simulation.getConfiguration().getName().replaceAll("cfg", Calendar.getInstance().getTimeInMillis() + ".rep"));
            reportDao.save(report);

            simulation.setReport(report);
            simulationDao.save(simulation);

            mailService.sendReport(simulation.getUser().getEmail(), report);
        }
    }
}
