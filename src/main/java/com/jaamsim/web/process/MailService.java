package com.jaamsim.web.process;

import com.jaamsim.web.model.simulation.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

/**
 * Created by tristano on 11/01/16.
 */
@Service
public class MailService {
    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private SimpleMailMessage simpleMailTemplateMessage;

    public void sendReport(String address, Report report) {

        MimeMessage message = javaMailSender.createMimeMessage();

        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setFrom(simpleMailTemplateMessage.getFrom());
            helper.setTo(address);
            helper.setSubject(String.format("%s %s", simpleMailTemplateMessage.getSubject(), report.getConfiguration().getName()));
            helper.setText(String.format("Simulation %s completed%nGenerated report %s", report.getConfiguration().getName(), report.getName()));

            helper.addAttachment(report.getName(), new ByteArrayDataSource(report.getFile(), "text/plain"));

        } catch(MessagingException e) {
            throw new MailParseException(e);
        }
        javaMailSender.send(message);
    }
}
