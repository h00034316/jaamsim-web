package com.jaamsim.web.dao;

import com.jaamsim.web.model.Configuration;
import com.jaamsim.web.model.simulation.Report;
import com.jaamsim.web.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by tristano on 06/01/16.
 */

public interface ConfigurationDao extends CrudRepository<Configuration, Integer> {

    List<Configuration> findAllByUser(User user);

    Configuration findByUserAndName(User user, String name);

    Configuration findByIdAndUser(Integer id,  User user);
}
