package com.jaamsim.web.dao;

import com.jaamsim.web.model.Configuration;
import com.jaamsim.web.model.User;
import com.jaamsim.web.model.simulation.Report;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by tristano on 06/01/16.
 */

public interface ReportDao extends CrudRepository<Report, Integer> {

    Report findByIdAndUser(int id, User user);

    List<Report> getAllByConfiguration(Configuration configuration);
}
