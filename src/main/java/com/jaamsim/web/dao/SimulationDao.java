package com.jaamsim.web.dao;

import com.jaamsim.web.model.Configuration;
import com.jaamsim.web.model.User;
import com.jaamsim.web.model.simulation.Simulation;
import com.jaamsim.web.model.simulation.State;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by tristano on 06/01/16.
 */

public interface SimulationDao extends CrudRepository<Simulation, Integer> {

    List<Simulation> findAllByUser(User user);

    List<Simulation> findAllByUserOrderByStartDesc(User user);

    List<Simulation> findByUserAndConfiguration(User user, Configuration configuration);

    List<Simulation> findByUserAndConfigurationOrderByStartDesc(User user, Configuration configuration);

    Simulation findByIdAndUser(Integer id, User user);

    Simulation findByUserAndConfigurationAndState(User user, Configuration configuration, State state);

    Simulation findByUserAndState(User user, State state);
}
