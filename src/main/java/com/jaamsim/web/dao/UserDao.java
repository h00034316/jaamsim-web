package com.jaamsim.web.dao;

import com.jaamsim.web.model.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by tristano on 28/11/15.
 */

public interface UserDao extends CrudRepository<User, Integer> {

    User findByEmail(String email);
}
