package com.jaamsim.web.interceptor;

import com.jaamsim.web.dao.UserDao;
import com.jaamsim.web.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by tristano on 28/12/15.
 */
public class LoginHandler extends HandlerInterceptorAdapter {

    private static final Logger LOG = LogManager.getLogger(LoginHandler.class);

    @Autowired
    private UserDao userDao;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        HttpSession session = request.getSession();

        User user = (User) session.getAttribute("user");
        if(user == null){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String userName = auth.getName();
            user = userDao.findByEmail(userName);
            session.setAttribute("user", user);

            LOG.info("Logging in as " + user.getEmail());
        }

        return super.preHandle(request, response, handler);
    }
}
