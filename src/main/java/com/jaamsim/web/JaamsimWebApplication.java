package com.jaamsim.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("/WEB-INF/applicationContext.xml")
public class JaamsimWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(JaamsimWebApplication.class, args);
	}
}

@Configuration
@ImportResource("/WEB-INF/jaamsim-security-spring.xml")
class XmlImportingSecurityConfiguration {
}