package com.jaamsim.web.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by tristano on 12/12/15.
 */
@Entity
@Table(name = "jaamsim_user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int userId;

    @NotNull
    private String email;

    @NotNull
    private String password;

    @Column(columnDefinition="tinyint(1) default 1")
    private boolean enabled;

    @Enumerated(EnumType.STRING)
    @Column(name = "jaamsim_role")
    private Role role;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
