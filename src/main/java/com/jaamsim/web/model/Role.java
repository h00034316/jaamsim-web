package com.jaamsim.web.model;

/**
 * Created by tristano on 12/12/15.
 */
public enum Role {
    ADMIN, USER
}