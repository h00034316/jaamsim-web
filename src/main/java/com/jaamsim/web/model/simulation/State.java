package com.jaamsim.web.model.simulation;

/**
 * Created by tristano on 12/12/15.
 */
public enum State {
    NEW,
    CONFIGURED,
    RUNNING,
    PAUSED,
    FINISHED,
    FAILED,
    ABORTED
}
