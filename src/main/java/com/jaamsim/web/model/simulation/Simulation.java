package com.jaamsim.web.model.simulation;

import com.jaamsim.web.model.Configuration;
import com.jaamsim.web.model.Group;
import com.jaamsim.web.model.User;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by tristano on 12/12/15.
 */
@Entity
@Table(name = "jaamsim_simulation")
public class Simulation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private int progress;
    private State state;
    private Date start;
    private Date end;

    @OneToOne
    private Configuration configuration;

    @OneToOne
    private Report report;

    // owner
    @OneToOne
    private User user;

    // sharing
    @OneToMany
    private List<User> users;

    // group sharing
    @OneToMany
    private List<Group> groups;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public Report getReport() {
        return report;
    }

    public void setReport(Report report) {
        this.report = report;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }
}
