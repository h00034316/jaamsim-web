package com.jaamsim.web.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by tristano on 12/12/15.
 */
@Entity
@Table(name = "jaamsim_group")
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToMany
    private List<User> users;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
