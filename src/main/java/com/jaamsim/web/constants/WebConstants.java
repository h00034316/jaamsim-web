package com.jaamsim.web.constants;

/**
 * Created by tristano on 06/01/16.
 */
public interface WebConstants {

    String WEB_REDIRECT = "redirect:";

    String WEB_JSP = "/jsp";
    String WEB_SIM = "/sim";

    String WEB_LOGIN = "/login";
    String WEB_MAIN = "/main";
    String WEB_CONFIGURATIONS = "/configurations";
    String WEB_CONFIGURATION = "/configuration";
}
