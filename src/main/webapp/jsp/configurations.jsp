<!--
* Base Bootstrap Layout Template downloaded from url
* http://getbootstrap.com/examples/dashboard/
* No Copyright found
-->

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

	<tags:header />

	<body>

		<tags:navbar />

		<div class="container-fluid">
			<div class="row">

				<tags:sidebar currentPage="configurations" />

				<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
					<h1 class="page-header">Simulation</h1>

					<div class="upload-configuration">
						<form method="POST" id="upload-form" action="${pageContext.request.contextPath}/sim/configurations/upload" enctype="multipart/form-data">
							<h2 class="sub-header">Upload configuration file</h2>

							<div class="row">
								<div class="col-sm-2">
									<input type="file" name="configurationFile" id="configurationFile" />
								</div>
							</div>
							<div class="row">
								<div class="progress" style="display: none">
									<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%"></div>
								</div>
							</div>
							<p />
							<div class="row">
								<div class="col-sm-2">
									<button type="submit" name="upload" id="upload" class="btn btn-lg btn-primary btn-block">Upload</button>
								</div>
							</div>
						</form>
					</div>

					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>Name</th>
									<th>Uploaded time</th>
									<th>Size <small>(KB)</small></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${configurations}" var="configuration" >
								<tr>
									<td><a href=${pageContext.request.contextPath}/sim/configuration/${configuration.id}>${configuration.name}</a></td>
									<td>${configuration.uploadedTime}</td>
									<td>${configuration.size}</td>
								</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<tags:bottom />

		<script src="${pageContext.request.contextPath}/js/jquery.form.min.js" rel="stylesheet" ></script>
		<script src="${pageContext.request.contextPath}/js/upload.js" rel="stylesheet" ></script>
	</body>
</html>
