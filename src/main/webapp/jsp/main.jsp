<!--
* Base Bootstrap Css Layout Template downloaded from url
* http://getbootstrap.com/examples/dashboard/
* No Copyright found
-->

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<tags:header />

	<body>

		<tags:navbar />

		<div class="container-fluid">
			<div class="row">
				<tags:sidebar />

				<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
					<h1 class="page-header">Dashboard</h1>

					<div class="row placeholders">
						<div class="col-xs-6 col-sm-3 placeholder">
							<img src="${pageContext.request.contextPath}/images/ScreenShotCafeModel.png" width="200" height="200" class="img-responsive" alt="Cafe Model">
							<h4>Cafe Model</h4>
							<span class="text-muted">Get more info</span>
						</div>
						<div class="col-xs-6 col-sm-3 placeholder">
							<img src="${pageContext.request.contextPath}/images/ScreenShotHarmonicOscillator.png" width="200" height="200" class="img-responsive" alt="Harmonic Oscillator">
							<h4>Harmonic Oscillator</h4>
							<span class="text-muted">Get statistics</span>
						</div>
						<div class="col-xs-6 col-sm-3 placeholder">
							<img src="${pageContext.request.contextPath}/images/ScreenShotFerrariDaytona.png" width="200" height="200" class="img-responsive" alt="Ferrari Daytona">
							<h4>Ferrari Daytona</h4>
							<span class="text-muted">Get more info</span>
						</div>
						<div class="col-xs-6 col-sm-3 placeholder">
							<img src="${pageContext.request.contextPath}/images/ScreenShotServerAndQueue.png" width="200" height="200" class="img-responsive" alt="Server And Queue">
							<h4>Server And Queue</h4>
							<span class="text-muted">Get more results</span>
						</div>
					</div>

					<h2 class="sub-header">Simulations</h2>
					<c:choose>
						<c:when test="${not empty simulations}">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Name</th>
											<th>Simulation Start Time</th>
											<th>Simulation End Time</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${simulations}" var="simulation">
											<tr>
												<td><a href="${pageContext.request.contextPath}/sim/configuration/${simulation.configuration.id}">${simulation.configuration.name}</a></td>
												<td>${simulation.start}</td>
												<td>${simulation.end}</td>
												<td>${simulation.state}</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</c:when>
						<c:otherwise>No simulations! Please go to configurations and run one!</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>

		<tags:bottom />
	</body>
</html>
