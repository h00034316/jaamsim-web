<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

    <tags:header />

    <body>

        <tags:navbar />

        <div class="container-fluid">
            <div class="row">

                <tags:sidebar currentPage="configurations" />

                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <h1 class="page-header">${configuration.name}</h1>

                    <br />

                    <c:choose>
                        <c:when test="${not empty currentSimulation}">
                            <h2 class="sub-header">Executing simulation ...</h2>
                            <div>
                                <div id="progressExecution" class="progress active">
                                    <div id="progress" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%"></div>
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <h2 class="sub-header">Run simulation</h2>
                            <p>
                            <form method="POST" action="${pageContext.request.contextPath}/sim/execute/${configuration.id}">
                                <table>
                                    <tr>
                                        <td><input type="submit" value="Run" name="execute" id="execute" /></td>
                                    </tr>
                                </table>
                            </form>
                            </p>
                        </c:otherwise>
                    </c:choose>

                    <c:if test="${not empty simulations}">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Start time</th>
                                    <th>End Time</th>
                                    <th>Status</th>
                                    <th>Report</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${simulations}" var="simulation" >
                                        <tr>
                                            <td>${simulation.id}</td>
                                            <td>${simulation.start}</td>
                                            <td>${simulation.end}</td>
                                            <td>${simulation.state}</td>
                                            <td><a href=${pageContext.request.contextPath}/sim/configuration/report/${simulation.report.id}>${simulation.report.name}</a></td>
                                        </tr>
                                   </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </c:if>

                    <h2 class="sub-header">Reports</h2>

                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Execution time</th>
                                    <th>Size <small>(KB)</small></th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:choose>
                                    <c:when test="${not empty reports}">
                                        <c:forEach items="${reports}" var="report" >
                                            <tr>
                                                <td><a href=${pageContext.request.contextPath}/sim/configuration/report/${report.id}>${report.name}</a></td>
                                                <td>${report.executionTime}</td>
                                                <td>${report.size}</td>
                                                <td>${report.state}</td>
                                            </tr>
                                        </c:forEach>
                                    </c:when>
                                    <c:otherwise>
                                        <tr><td>No reports!</td></tr>
                                    </c:otherwise>
                                </c:choose>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <tags:bottom />

        <c:if test="${not empty currentSimulation}">
            <script language="javascript">

                $(document).ready(function(){
                    var progressExecution = setInterval(function(){
                        $.get("${pageContext.request.contextPath}/sim/executing/${configuration.id}", function(data){
                            $("#progress").css('width', data.progress + '%');
                            $("#progress").html(data.progress + '%');

                            if(data.progress > 99.999 || data.executing == false) {
                                clearInterval(progressExecution);
                                $("#progressExecution").removeClass("active");
                                $("#progress").html("Done");
                                location.reload(true);
                            }
                        })
                    }, 1000);
                });
            </script>
        </c:if>
    </body>
</html>
