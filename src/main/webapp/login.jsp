<!--
* Base Bootstrap Layout Template downloaded from url
* http://getbootstrap.com/examples/signin/
* No Copyright found
-->


<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="favicon.ico">

	<title>JaamSim in the Cloud</title>

	<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<link href="${pageContext.request.contextPath}/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>

<div class="container">

	<form class="form-signin col-md-4 col-md-offset-4" method="POST" action="j_spring_security_check">
		<h3 class="form-signin-heading">Login to JaamSim in the Cloud</h3>
		<c:if test="${not empty param.error}">
			<div class="alert alert-danger">Username or Password not valid!</div>
		</c:if>

		<div class="form-group">
			<label for="j_username">User</label>
			<input type="email" id="j_username" name="j_username" class="form-control" placeholder="user@someEmail.com" required autofocus>
		</div>

		<div class="form-group">
			<label for="j_password">Password</label>
			<input type="password" id="j_password" name="j_password" class="form-control" placeholder="********" required>
		</div>

		<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
	</form>

</div>

</body>
</html>
