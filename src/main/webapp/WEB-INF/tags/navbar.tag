<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
* Base Bootstrap Css Layout Template downloaded from url
* http://getbootstrap.com/examples/dashboard/
* No Copyright found
--%>

<nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Jaamsim in the Cloud</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="${pageContext.request.contextPath}/sim/main">Dashboard</a></li>
                        <li><a href="#">Settings <small>(under development)</small></a></li>
                        <li><a href="#">Profile <small>(under development)</small></a></li>
                        <li><a href="#">Help <small>(under development)</small></a></li>
                        <li><a href="<c:url value="/j_spring_security_logout" />">Logout</a></li>
                    </ul>
                    <form class="navbar-form navbar-right">
                        <input type="text" class="form-control" placeholder="Search (under development)...">
                    </form>
                </div>
            </div>
        </nav>
