<%--
* Base Bootstrap Css Layout Template downloaded from url
* http://getbootstrap.com/examples/dashboard/
* No Copyright found
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="currentPage" required="false" type="java.lang.String" %>

                <div class="col-sm-3 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">
                        <li class="${empty currentPage or currentPage == 'main' ? 'active' : ''}"><a href="${pageContext.request.contextPath}/sim/main">Overview <span class="sr-only">(current)</span></a></li>
                        <li class="${currentPage == 'configurations' ? 'active' : ''}"><a href="${pageContext.request.contextPath}/sim/configurations">Simulation</a></li>
                        <li><a href="#">Modelling <small>(under development)</small></a></li>
                        <li><a href="#">Analytics <small>(under development)</small></a></li>
                        <li><a href="#">Export <small>(under development)</small></a></li>
                    </ul>
                </div>
