/**
 * Created by tristano on 06/01/16.
 */

beforeSubmitHandler = function(arr, form, options) {
    $.each(arr, function (index, inputField) {
        if ('configurationFile' === inputField.name && inputField.value === "") {
            return false;
        }
        $('.progress').fadeIn();
        return true;
    });
};

successHandler = function(responseText, statusText, xhr, form) {
    $('.upload-configuration .progress-bar').width('100%');
    $('.upload-configuration .progress-bar').html('100%');
    $('.progress').delay(1000).fadeOut('fast');
    location.reload(true);
};

handleUploadProgress = function (event, position, total, percentComplete) {
    $('.upload-configuration .progress-bar').width(percentComplete + '%');
    $('.upload-configuration .progress-bar').html(percentComplete + '%');
};

$(document).ready(function() {
    $("#upload-form").ajaxForm({
        beforeSubmit: beforeSubmitHandler,
        success: successHandler,
        clearForm: true,
        uploadProgress: handleUploadProgress
    });
});
